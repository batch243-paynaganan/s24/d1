// console.log("hello world")

// [Section] Exponent operator;

// before ES6
const firstNum = 8 ** 2;
console.log(firstNum)

// ES6
const secondNum = Math.pow(8, 2);
console.log(secondNum)

/*[Section] Template literals */
/*
	allows us to write strings without using concatination operator(+)
	for readability purposes, greatly helps with code readability.
*/

let name = 'John';
	// before template literal string
	// use single/double quote
	let message = 'Hello ' + name + '! Welcome to programming!';
	console.log(message)

	// Using template literals
	// uses back ticks(``);
	message = `Hello ${name}! Welcome to programming!`
	console.log(message)

	// Template literals allow us to write strings with embedded javascript expressions
	const interestRate = 0.1;
	const principal = 1000;
	console.log(`The interest on your savings account is: ${interestRate*principal}`)

	// [Section] Array Destructuring
	/*
		allows us to unpack elements in arrays into distinct variables
		allows us to name array elements with variables instead of using index numbers
		it will help us with code readability
		syntax: let/const [variableNameA, variableNameB, ...] = arrayName;
	*/

	const fullName = ['Juan', 'Dela', 'Cruz']
	// before array destructuring
	console.log(fullName[0]);
	console.log(fullName[1]);
	console.log(fullName[2]);
	console.log(`Hello ${fullName[0]} ${fullName[1]} ${fullName[2]}, nice to meet you!`);

	// array destructuring
	const [firstName, middleName, lastName] = fullName;
	console.log(firstName)
	console.log(middleName)
	console.log(lastName)

	console.log(fullName)
	console.log(`Hello ${firstName} ${middleName} ${lastName}! It's nice to meet you!`)

	// [Section] Object Destructuring
		/*
			it allows us to unpack properties of objects into distinct variables
			syntax: let/const{ propertyNameA, propertyNameB, propertyNameC, ...} = object name;
		*/

	const person = {
		givenName: 'Jane',
		maidenName: 'Dela',
		familyName: 'Cruz'
	}

	// Before the object 
		console.log(person.givenName);
		console.log(person['maidenName']);
		console.log(person.familyName)

	// Object destructuring
		let {givenName, maidenName, familyName} = person;
		console.log(givenName)
		console.log(maidenName)
		console.log(familyName)
		console.log(person)

	function getFullName({givenName, maidenName, familyName}){
		console.log(`${givenName} ${maidenName} ${familyName}`)
	}

	getFullName(person);

// [Section] Arrow functions
	// compact alternative syntax to traditional functions.
	// useful for code snippets where creating functions will not be reused in any other other portion of the code

	/*const hello = () => {
		console.log("Hello world from anonymous function")
	}*/

	/*const hello = () => console.log("Hello world from anonymous function")*/
	/*hello();*/

	const hello = (x,y) => x+y;
	/*const hello = function(){
		console.log("Hello world")
	}
*/
	console.log(hello(5,6));


	// before arrow function and template literals
	function printFullName(firstName, middleInitial, lastName)
		{
			console.log(firstName + " " + middleInitial + " " + lastName);
		}
	printFullName("Mark", "B", "Payns")

	// arrow function
	let fName = (firstName, middleInitial, lastName) =>{
		console.log(`${firstName} ${middleInitial} ${lastName}`)
	}

	fName("Mark", "B", "Payns")

	// arrow functions with loops

	const student = ['John', 'Jane', 'Judy'];
	// before arrow function

	function iterate(student){
		console.log(student + " is a student!")
	}
	student.forEach(iterate);

	// arrow function
	/*let students = (student) => {
		console.log(`${student} is a student`)
	}
	student.forEach(students)*/

	student.forEach(student => console.log(`${student} is a student`));

	// [Section] Implicit Return Statement
		/*
			-there are instances when you can omit return statement
			-this works because even without return statement javascript implicitly adds it for the result of function
		*/


	const add = (x,y) => {
		console.log(x+y);
		return x+y;
	}

	let sum = add(23,35);
	console.log(`This is the sum contained in some variable`)
	console.log(sum);

	const subtract = (x,y) => x-y;

	subtract(10,5);
	let difference = subtract(10,5);
	console.log(difference)

// [Section] Default Function argument value
	// provide a default argument value if none provided when the function is invoked

	const greet = (name = "User") =>{
		return `Good morning, ${name}!`;
	}

	console.log(greet())

// [Section] Class-based object blueprints
	// allows us to create/instantiation using classes blueprints
	// creating a class
		// constructor is a special method of a class for creating/initializing an object for that class
	/*
		syntax:
		class className{
			constructor (objectvalueA, objectvalueB, ...){
				this.objectProperty = objectValueA;
				this.objectProperty = objectValueB;
			}
		}
	*/

	class Car{
		constructor(brand, name, year){
			this.carBrand = brand;
			this.carName = name;
			this.carYear = year;
		}
	}
	let car = new Car('Toyota', 'Hilux-pickup', '2015')
	console.log(car);

	car.carBrand = 'Nissan'
	console.log(car)



